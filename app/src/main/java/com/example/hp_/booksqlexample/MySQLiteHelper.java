package com.example.hp_.booksqlexample;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

public class MySQLiteHelper extends SQLiteOpenHelper {
	
	// Database Version
    private static final int DATABASE_VERSION = 2;
    // Database Name
    private static final String DATABASE_NAME = "ContactDB";
   
	public MySQLiteHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);	
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// SQL statement to create contact table
		String CREATE_Contact_TABLE = "CREATE TABLE contacts ( " +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, " + 
				"name TEXT, "+
				"number TEXT )";
		
		// create contacts table
		db.execSQL(CREATE_Contact_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Drop older contacts table if existed
        db.execSQL("DROP TABLE IF EXISTS contacts");
        
        // create fresh contacts table
        this.onCreate(db);
	}
	//---------------------------------------------------------------------
   
	/**
     * CRUD operations (create "add", read "get", update, delete) contact + get all contacts + delete all contacts
     */

	// Contacts table name
    private static final String TABLE_CONTACTS = "contacts";
    
    // Contacts Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    private static final String KEY_NUMBER = "number";
    
    private static final String[] COLUMNS = {KEY_ID,KEY_NAME,KEY_NUMBER};

	public void addContact(Contact contact){

		// 1. get reference to writable DB
		SQLiteDatabase db = this.getWritableDatabase();

		// 2. create ContentValues to add key "column"/value
        ContentValues values = new ContentValues();
        values.put(KEY_NAME, contact.getName()); // get title
        values.put(KEY_NUMBER, contact.getNumber()); // get author
 
        // 3. insert
        db.insert(TABLE_CONTACTS, // table
        		null, //nullColumnHack
        		values); // key/value -> keys = column names/ values = column values
        
        // 4. close
        db.close(); 
	}
	

	
	// Get All contacts
    public ArrayList<Contact> getAllContacts() {
        ArrayList<Contact> contacts = new ArrayList<Contact>();

        // 1. build the query
        String query = "SELECT  * FROM " + TABLE_CONTACTS;
 
    	// 2. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
 
        // 3. go over each row, build contact and add it to list
        Contact contact = null;
        if (cursor.moveToFirst()) {
            do {
                contact = new Contact();
                contact.setId(Integer.parseInt(cursor.getString(0)));
                contact.setName(cursor.getString(1));
                contact.setNumber(cursor.getString(2));

                // Add contact to contacts
                contacts.add(contact);
            } while (cursor.moveToNext());
        }
        
		Log.d("getAllContacts()", contacts.toString());

        // return contacts
        return contacts;
    }





    // Deleting single contact
    public void deleteContact(Contact contact) {

    	// 1. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();
        
        // 2. delete
        db.delete(TABLE_CONTACTS,
        		KEY_ID+" = ?",
                new String[] { String.valueOf(contact.getId()) });
        
        // 3. close
        db.close();
        
		Log.d("deleteContact", contact.toString());

    }
}
