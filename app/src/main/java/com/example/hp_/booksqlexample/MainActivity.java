package com.example.hp_.booksqlexample;

import android.app.Activity;
import android.os.Bundle;

import com.example.hp_.booksqlexample.R;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		
		MySQLiteHelper db = new MySQLiteHelper(this);
        

        // add contacts
        db.addContact(new Contact("contact 1", "01236495"));
        db.addContact(new Contact("contact 2", "01236454"));
        db.addContact(new Contact("contact 3", "01556454"));
        
        // get all contacts
        ArrayList<Contact> list = db.getAllContacts();



        // delete one contact
        db.deleteContact(list.get(0));
        
        // get all contacts
        db.getAllContacts();

        
	}

}
